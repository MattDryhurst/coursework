**Deployment steps**
1. Run a VM with the IP address 192.168.56.11 and enough CPU power to handle multiple containers
2. Unzip file in VM (alternatively run command: git clone https://gitlab.com/MattDryhurst/coursework.git )
3. change directory into the folder you unzipped (run command: cd coursework/ )
4. run the docker compose ( use command: sudo docker-compose up --build)
5. Go to Rabbit MQ at http://192.168.56.11:15672/ and add user test with password test that has admin access
5. Wait for all containers to start or rerun sudo docker-compose up --build if any rabbitmq username related errors

**How it works**
A__

1. Each js node will send a message to Rabbit MQ at regular intervals including there own nodeID
2. Each js node will read from Rabbit MQ to see what the other nodes nodeID is 
3. After a period of time and all nodes have sent and recoeved each others message there will be an election
4. The node with the largest node id is now elected as the leader
5. The leader will now periodically check the health of the other containers using Docker API
6. All nodes will continue creating new node IDs and talking to each other 
7. Periodically a new election will happen and a new node will become the leader
8. When the leader notices a node is down it will call Docker API and create then start the node that died
9. If there has not been a container health check in a long time the leader must be dead and a non leader will call Docker API to regenerate the node
10. Leader elections will continue once all 3 nodes are up

B__

1.The entire time all nodes are listening on port 80, 81, 82 for POST from notFLIX
2. When a POST request comes in it is formatted and sent into the mongoDB
3. Mongo will then store this in the volumes
4. database entries can be read using a GET request

**Testing**
1. Watch the logs for 15 minutes and you will see nodes talking to each other via Rabbit MQ
2. Watch the logs and you can see elections happen
3. Watch the logs and you can see only the leader is doing container health checks
4. Kill a none leader node and you will see the leader bring up the dead node
5. Kill the leader and you will see no container check for 10 minutes and then the leader will be brought back up. Shortly a new leader will be announced and all nodes will be back
6. POST a request to the database. example in ..DB/express.http 
7. Send a GET request to view details in the databse. example in ..DB/express.http 


//********************************DATABASE*********************************//
//Pull in mongoose library for use with db
const mongoose = require('mongoose');

//Pull in express library for use with web
const express = require('express');

//Pull in body-parser library to parse the server response from json to object.
const bodyParser = require('body-parser');

//instance of express and port to use for inbound connections.
const app = express()
const port = 3000

//connection string listing the mongo servers.
//This is used instead of a load balancer
const connectionString = 'mongodb://localmongo1:27017,localmongo2:27017,localmongo3:27017/NotFLIXdb?replicaSet=cfgrs';

//tell express to use the body parser. Note - This function was built into express but then moved to a seperate package.
app.use(bodyParser.json());

//connect to the cluster
mongoose.connect(connectionString, { useNewUrlParser: true, useUnifiedTopology: true });
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

//Setting the db schema to match the details required by spec
var Schema = mongoose.Schema;
var insightSchema = new Schema({
  accountid: Number,
  datetime: Date,
  pointofinteraction: String,
  titleid: Number,
  typeofinteraction: String,
  useraction: String,
  username: String
});
var insightModel = mongoose.model('Insight', insightSchema, 'insight');

//GET request will return all usernames currently in the database
app.get('/', (req, res) => {
  insightModel.find({}, 'username', (err, insight) => {
    if (err) return handleError(err);
    res.send(JSON.stringify(insight))
  })
})

//POST request takes in JSON with all of the schema as parameters and inserts into DB
app.post('/', (req, res) => {
  var new_insight_instance = new insightModel(req.body);
  new_insight_instance.save(function (err) {
    if (err) res.send('Error');
    res.send(JSON.stringify(req.body)
    );
    console.log(JSON.stringify(req.body));
  });
})

//bind the express web service to the port specified
app.listen(port, () => {
  console.log(`Express Application listening at port ` + port)
})

//This is used for development purposes to make sure application is still up
setInterval(function () {
  console.log(`Intervals are used to fire a function every 10 seconds for the lifetime of an application.`);
  if (leader == 1){
    console.log("Also im the leader");
  }
}, 10000);
//**************************************************************************//






//********************************MESSAGING*********************************//
//Bringing in amqplib library so it can subscribe and publish messages to the queue
const rabbitMQPublishMessage = require('amqplib/callback_api');
const rabbitMQSubscribeMessage = require('amqplib/callback_api');

//Get the hostname of the node
var os = require("os");
var myhostname = os.hostname();
const fs = require('fs');
nodesTxtFile = fs.readFileSync('node.js');
nodes = JSON.parse(nodesTxtFile);

//print the hostname
console.log(myhostname);
var nodeArray = new Map();

//Default is that the system has no leader
systemLeader = 0

//default is that you are not the leader
leader = 0;

//Call publish to get the ball rolling
publish();

//Call subscribe to check everyone can talk
subscribe();

//If there is no leader... elect one
if (systemLeader == 0) {
  leadershipElection();
}

//utility to get map key by the value
function getByValue(map, searchValue) {
  for (let [key, value] of map.entries()) {
    if (value === searchValue)
      return key;
  }
}

function leadershipElection() {
  leader = 0;
  console.log("Current hostnames and corresponding id. Hostname with largest ID will become leader");
  console.log(nodeArray);

  //Get the largest nodeID
  var leaderNodeID = Math.max(...nodeArray.values());

  //Use the largest nodeID getLeaderHostname
  var leaderHostname = getByValue(nodeArray, leaderNodeID);

  //if i am the leader. Update leader boolean to true
  if (leaderHostname == myhostname){
    console.log("I " + myhostname + " is the leader now");
    leader = 1;
  }else{
    leader = 0;
  }
}

function subscribe() {
  rabbitMQSubscribeMessage.connect('amqp://test:test@192.168.56.11', function (error0, connection) {
    if (error0) {
      throw error0;
    }
    connection.createChannel(function (error1, channel) {
      if (error1) {
        throw error1;
      }
      var exchange = 'logs';

      channel.assertExchange(exchange, 'fanout', {
        durable: false
      });

      channel.assertQueue('', {
        exclusive: true
      }, function (error2, q) {
        if (error2) {
          throw error2;
        }
        console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q.queue);
        channel.bindQueue(q.queue, exchange, '');

        channel.consume(q.queue, function (msg) {
          if (msg.content) {
            console.log(" [x] %s", msg.content.toString());
            var msgInJSON = JSON.parse(msg.content.toString());
            nodeArray.set(msgInJSON.hostname, msgInJSON.nodeID);
          }
        }, {
          noAck: true
        });
      });
    });
  });

}

function publish() {
  //Give the node a random number
  var nodeID = Math.floor(Math.random() * (100 - 1 + 1) + 1);

  //build message to send to Rabbit
  var msg = { "hostname": myhostname, "status": "alive", "nodeID": nodeID };

    console.log(myhostname + "is alive");

    //publish
    rabbitMQPublishMessage.connect('amqp://test:test@192.168.56.11', function (error0, connection) {
      if (error0) {
        throw error0;
      }

      connection.createChannel(function (error1, channel) {
        if (error1) {
          throw error1;
        }
        var exchange = 'logs';

        channel.assertExchange(exchange, 'fanout', {
          durable: false
        });

        channel.publish(exchange, '', Buffer.from(JSON.stringify(msg)));
        console.log(" [x] Sent %s", msg);
      });
      setTimeout(function () {
        connection.close();
      }, 500);


    });
}





//**************************************************************************//

//call the functions//

//call pub every 10 seconds
setInterval(function () {
  publish();
}, 20000);

//call sub every 5 seconds
setInterval(function () {
  subscribe();
}, 20000);

//call leadership every 30 seconds
setInterval(function () {
  leader = 0;
  leadershipElection();
}, 50000);